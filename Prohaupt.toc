\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Interferenz}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fresnel-Huygens-Prinzip}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Fraunhofer-Beugung}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}L\IeC {\"o}sungen der Intensit\IeC {\"a}tsverteilung f\IeC {\"u}r die verschiedenen Objekte}{2}{subsection.2.4}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Spalt}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Steg}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Gitter}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Lochblende}{10}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Doppelloch}{12}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Wellenl\IeC {\"a}nge des He-Ne lasers}{14}{subsection.4.6}
\contentsline {section}{\numberline {5}Diskussion}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Beugungsmuster}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Wellenl\IeC {\"a}nge und Genauigkeit}{15}{subsection.5.2}
\contentsline {section}{\nonumberline Literatur}{16}{section*.19}
