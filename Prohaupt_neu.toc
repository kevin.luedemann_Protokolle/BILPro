\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Interferenz}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fresnel-Huygens-Prinzip}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Fraunhofer-Beugung}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}L\IeC {\"o}sungen der Intensit\IeC {\"a}tsverteilung f\IeC {\"u}r die verschiedenen Objekte}{2}{subsection.2.4}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {section}{\numberline {5}Diskussion}{4}{section.5}
\contentsline {section}{\nonumberline Literatur}{7}{section*.9}
