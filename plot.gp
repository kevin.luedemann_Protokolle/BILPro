reset 

set terminal epslatex color

set xlabel 'Position [\si{\milli\meter}]'
set ylabel 'Spannung [\si{\volt}]'

set output 'doppelloch.tex'
p 'Doppelloch01x10.dat' u ($1/400):($2/10) t 'Messwerte'
set output
!epstopdf doppelloch.eps


set output 'gitter.tex'
p 'Gitter08.dat' u ($1/400):2 t 'Messwerte', 'Gitter09X10.dat' u ($1/400):($2/10) lt 1 notitle, 'Gitter10X10.dat' u ($1/400):($2/10) lt 1 notitle, 'Gitter11X10.dat' u ($1/400):($2/10) lt 1 notitle, 'Gitter12X10.dat' u ($1/400):($2/10) lt 1 notitle
set output
!epstopdf gitter.eps


set output 'loch.tex'
p 'Loch01x10.dat' u ($1/400):($2/10) t 'Messwerte'
set output
!epstopdf loch.eps


set output 'spalt.tex'
p 'Spalt02.dat' u ($1/400):2 t 'Messwerte', 'Spalt03x10.dat' u ($1/400):($2/10) lt 1 notitle, 'Spalt04x10.dat' u ($1/400):($2/10) lt 1 notitle
set output
!epstopdf spalt.eps


set output 'steg.tex'
p 'Steg03x10.dat' u ($1/400):($2/10) lt 1 t 'Messwerte', 'Steg04x10.dat' u ($1/400):($2/10) lt 1 notitle, 'Steg05.dat' u ($1/400):2 lt 1 notitle
set output
!epstopdf steg.eps

set key left
f(x)=m*x+b

set ylabel '$\alpha \times10^{-3}\;[\si{\degree}]$'
set xlabel '$\varepsilon/\pi\;[\text{rad}]$'

set output 'doppelloch_ext.tex'
set fit logfile 'doppelloch_ext.log'
fit f(x) 'dext.dat' u 1:(($2-17335)/400/1.149):(0.5/1.149) via m,b
p f(x) t 'Refression', 'dext.dat' u 1:(($2-17335)/400/1.149):(0.5/1.149) w e t 'Doppelloch'
set output
!epstopdf doppelloch_ext.eps


set output 'gitter_ext.tex'
set fit logfile 'gitter_ext.log'
fit f(x) 'gext.dat' u 1:(($2-16980)/400/1.149):(0.5/1.149) via m,b
p 'gext.dat' u 1:(($2-16980)/400/1.149):(0.5/1.149) w e t 'Gitter', f(x) t 'Regression'
set output
!epstopdf gitter_ext.eps


set output 'loch_ext.tex'
set fit logfile 'loch_ext.log'
fit f(x) 'lext.dat' u 1:(($2-17327)/400/1.149):(0.5/1.149) via m,b
p 'lext.dat' u 1:(($2-17327)/400/1.149):(0.5/1.149) w e t 'Loch', f(x) t 'Regression'
set output
!epstopdf loch_ext.eps


set output 'spalt_ext.tex'
set fit logfile 'spalt_ext.log'
fit f(x) 'sext.dat' u 1:(($2-16915)/400/1.149):(0.5/1.149) via m,b
p 'sext.dat' u 1:(($2-16915)/400/1.149):(0.5/1.149) w e t 'Spalt', f(x) t 'Regression'
set output
!epstopdf spalt_ext.eps


set output 'steg_ext.tex'
set fit logfile 'steg_ext.log'
fit f(x) 'stext.dat' u 1:(($2-16785)/400/1.149):(0.5/1.149) via m,b
p 'stext.dat' u 1:(($2-16785)/400/1.149):(0.5/1.149) w e t 'Steg', f(x) t 'Regression'
set output
!epstopdf steg_ext.eps
