reset 

set terminal epslatex color

set output 'doppelloch.tex'
p 'Doppelloch01x10.dat' u 1:($2/10) t 'Messwerte'
set output
!epstopdf doppelloch.eps


set output 'gitter.tex'
p 'Gitter08.dat' u 1:2 t 'Messwerte', 'Gitter09X10.dat' u 1:($2/10) lt 1 notitle, 'Gitter10X10.dat' u 1:($2/10) lt 1 notitle, 'Gitter11X10.dat' u 1:($2/10) lt 1 notitle, 'Gitter12X10.dat' u 1:($2/10) lt 1 notitle
set output
!epstopdf gitter.eps


set output 'loch.tex'
p 'Loch01x10.dat' u 1:($2/10) t 'Messwerte'
set output
!epstopdf loch.eps


set output 'spalt.tex'
p 'Spalt01.dat' u 1:2 t 'Messwerte', 'Spalt02.dat' u 1:2 lt 1 notitle, 'Spalt03x10.dat' u 1:($2/10) lt 1 notitle, 'Spalt04x10.dat' u 1:($2/10) lt 1 notitle
set output
!epstopdf spalt.eps


set output 'steg.tex'
p 'Steg01.dat' u 1:2 t 'Messwerte', 'Steg02.dat' u 1:2 lt 1 notitle, 'Steg03x10.dat' u 1:($2/10) lt 1 notitle, 'Steg04x10.dat' u 1:($2/10) lt 1 notitle, 'Steg05.dat' u 1:2 lt 1 notitle
set output
!epstopdf steg.eps


set output 'doppelloch_ext.tex'
p 'dext.dat' u ($1-17335):($2/400/114.9)
set output
!epstopdf doppelloch_ext.eps
